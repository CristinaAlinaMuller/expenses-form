package alina.project.expense.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.TemporalType.DATE;

@Entity(name = "ExpenseReport")
@Table(name = "expenseReports")
public class ExpenseReport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd", iso = DateTimeFormat.ISO.DATE)

    private LocalDate date;

    private Double advance;

    @DateTimeFormat(pattern = "yyyy-MM-dd", iso = DateTimeFormat.ISO.DATE)
    @Temporal(DATE)
    private Date startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd", iso = DateTimeFormat.ISO.DATE)
    @Temporal(DATE)
    private Date endDate;

    private int noOfDays;

    private int leaveValue;

    private Double totalDocs;

    @OneToMany(mappedBy = "expenseReport", fetch = FetchType.LAZY,
            cascade = CascadeType.MERGE, orphanRemoval = true)
    private List<Document> documents = new ArrayList<>();

    private Double total;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Double getAdvance() {
        return advance;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(int noOfDays) {
        this.noOfDays = noOfDays;
    }

    public int getLeaveValue() {
        return leaveValue;
    }

    public void setLeaveValue(int totalValue) {
        this.leaveValue = totalValue;
    }

    public Double getTotalDocs() {
        return totalDocs;
    }

    public void setTotalDocs(Double totalDocs) {
        this.totalDocs = totalDocs;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }


    public void addDocument(Document document) {
        documents.add(document);
        document.setExpenseReport(this);
    }

    public void removeDocument(Document document) {
        documents.remove(document);
        document.setExpenseReport(null);
    }

    public void deleteDocuments() {
        this.getDocuments().removeAll(documents);

    }

}
