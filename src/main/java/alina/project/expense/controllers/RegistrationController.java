package alina.project.expense.controllers;

import alina.project.expense.models.User;
import alina.project.expense.models.UserRegistrationDto;
import alina.project.expense.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static alina.project.expense.controllers.RegistrationController.Routes.*;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UserService userService;


    @ModelAttribute("user")
    public UserRegistrationDto getUserRegistrationDTO() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String getUserRegistrationForm() {
        return REGISTRATION_ROOT;
    }

    @PostMapping
    public String registerUserReportount(@ModelAttribute("user") @Valid UserRegistrationDto dto,
                                      BindingResult bindingResult,
                                      Model model) {
        User existingUser = userService.findByLogin(dto.getLogin());
        if (existingUser != null) {
            bindingResult.rejectValue("login", "403", "there already is an user with that username");
        }

        if (bindingResult.hasErrors()) {
            return REGISTRATION_ROOT;
        }

        userService.save(dto);

        return REGISTRATION_SUCCESS;
    }

    static class Routes {
        static final String REGISTRATION_ROOT = "registration";
        static final String REGISTRATION_SUCCESS = "redirect:/registration?success";
    }
}
