package alina.project.expense.controllers;

import alina.project.expense.models.ExpenseReport;
import alina.project.expense.repositories.ExpenseReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;


@RestController
public class AjaxController {

    @Autowired
    private ExpenseReportRepository expenseReportRepository;


    @GetMapping("/save_advance")
    public Double saveAjaxInfoAdvance(@RequestParam("id") String id, @RequestParam("advance") String advance) {

        ExpenseReport currentExpenseReport = expenseReportRepository.findById(Long.valueOf(id)).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        currentExpenseReport.setAdvance(Double.valueOf(advance));

        expenseReportRepository.save(currentExpenseReport);

        return Double.valueOf(advance);
    }

    @GetMapping("/save_date")
    public LocalDate saveAjaxInfoDate(@RequestParam("id") String id, @RequestParam("date") String date) throws ParseException {

        ExpenseReport currentExpenseReport = expenseReportRepository.findById(Long.valueOf(id)).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateD = LocalDate.parse(date, formatter);
        currentExpenseReport.setDate(dateD);
        expenseReportRepository.save(currentExpenseReport);

        return dateD;
    }

    @GetMapping("/save_leave")
    public int saveAjaxInfoLeaveVal(@RequestParam("id") String id,
                                    @RequestParam("startD") String start,
                                    @RequestParam("endD") String end,
                                    @RequestParam("leaveVal") String leaveVal) throws ParseException {

        ExpenseReport currentExpenseReport = expenseReportRepository.findById(Long.valueOf(id)).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        Date startD = new SimpleDateFormat("yyyy-MM-dd").parse(start);
        Date endD = new SimpleDateFormat("yyyy-MM-dd").parse(end);
        currentExpenseReport.setStartDate(startD);
        currentExpenseReport.setEndDate(endD);
        currentExpenseReport.setLeaveValue(Integer.parseInt(leaveVal));
        Double total = currentExpenseReport.getTotal();
        currentExpenseReport.setTotal(total- currentExpenseReport.getLeaveValue());
        expenseReportRepository.save(currentExpenseReport);

        return Integer.parseInt(leaveVal);
    }
}
