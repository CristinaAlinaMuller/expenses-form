package alina.project.expense.controllers;

import alina.project.expense.models.Document;
import alina.project.expense.models.ExpenseReport;
import alina.project.expense.repositories.DocumentRepository;
import alina.project.expense.repositories.ExpenseReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static alina.project.expense.controllers.ContentController.Routes.*;
import static alina.project.expense.controllers.UserController.Routes.REDIRECT;

@Controller
public class DocumentController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentController.class);

    @Autowired
    private ExpenseReportRepository expenseReportRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @GetMapping("/add-document/{id}")
    public String getAddDocument(@PathVariable("id") Long eId,
                                 Model model,
                                 Document document) {

        ExpenseReport existingExpenseReport = expenseReportRepository.findById(eId).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", eId)));
        });

        model.addAttribute("expenseReport", existingExpenseReport);
        model.addAttribute("document", document);

        return ADD_DOCUMENT;
    }

    @PostMapping("/save-document/{id}")
    public String createNewDocument(@PathVariable("id") Long eId,
                                    @Valid Document document,
                                    BindingResult result,
                                    Model model) {

        if ((result.hasErrors())) {
            return ADD_DOCUMENT;
        }

        ExpenseReport existingExpenseReport = expenseReportRepository.findById(eId).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", eId)));
        });

        document.setExpenseReport(existingExpenseReport);
        documentRepository.save(document);
        existingExpenseReport.getDocuments().add(document);

        existingExpenseReport.setTotalDocs(existingExpenseReport.getTotalDocs() + document.getValue());

        populateDocuments(model);
        populateExpenseReports(model);

        return String.format("%s/%s/%s", REDIRECT, ADD_EXPENSE_ACC, existingExpenseReport.getId());
    }

    @GetMapping("/edit-doc/{id}")
    public String getEditDocPage(@PathVariable("id") Long id, Document document, Model model) {

        Document existingDoc = documentRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing document with id: %s", id)));
        });

        model.addAttribute("document", existingDoc);
        model.addAttribute("documents", documentRepository.findAll());

        return EDIT_DOC;
    }

    @PostMapping("/update-doc/{id}")
    public String updateDocReturnToExp(@PathVariable("id") Long id,
                                       Document document,
                                       Model model,
                                       BindingResult result) {

        if (result.hasErrors()) {
            return EDIT_DOC;
        }
        populateDocuments(model);
        populateExpenseReports(model);

        Long expId = documentRepository.findById(id).get().getExpenseReport().getId();
        ExpenseReport existingExpenseReport = expenseReportRepository.findById(expId).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        document.setExpenseReport(existingExpenseReport);
        documentRepository.save(document);

        expenseReportRepository.save(existingExpenseReport);

        return String.format("%s/%s/%s", REDIRECT, ADD_EXPENSE_ACC, existingExpenseReport.getId());
    }

    @GetMapping("/delete-doc/{id}")
    public String deleteDoc(@PathVariable("id") Long id, Model model) {

        Long expId = documentRepository.findById(id).get().getExpenseReport().getId();

        documentRepository.deleteById(id);

        model.addAttribute("documents", documentRepository.findAll());

        return String.format("%s/%s/%s", REDIRECT, ADD_EXPENSE_ACC, expId);
    }

    private void populateExpenseReports(Model model) {
        List<ExpenseReport> expenseReports = expenseReportRepository.findAll();
        if (!expenseReports.isEmpty()) {
            model.addAttribute("expenseReports", expenseReports);
        }
    }

    private void populateDocuments(Model model) {
        List<Document> documents = documentRepository.findAll();
        if (!documents.isEmpty()) {
            model.addAttribute("documents", documents);
        }
    }

    static class Routes {
        static final String ADD_EXPENSE_ACC = "add-expenseReport";
        static final String REDIRECT = "redirect:";
        static final String ADD_DOCUMENT = "add-document";
        static final String EDIT_DOC = "edit-doc";
        static final String UPDATE_DOC = "update-doc";
    }
}
