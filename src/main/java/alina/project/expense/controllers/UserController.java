package alina.project.expense.controllers;

import alina.project.expense.models.ExpenseReport;
import alina.project.expense.models.User;
import alina.project.expense.repositories.ExpenseReportRepository;
import alina.project.expense.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static alina.project.expense.controllers.UserController.Routes.*;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExpenseReportRepository expenseReportRepository;


    @GetMapping
    public String getIndexPage(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<ExpenseReport> userExpenseReports = userRepository.findByLogin(auth.getName()).getExpenseReports();
        //Long userId  = userRepository.findByLogin(auth.getName()).getId(); - varianta de cautare prin userId identificat
        //List<ExpenseReport> userExpenseReports = userRepository.findById(userId).get().getExpenseReports();
        if (!userExpenseReports.isEmpty()) {
            model.addAttribute("userExpenseReports", userExpenseReports);
        }

        model.addAttribute("users", userRepository.findAll());

        return INDEX;
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id, Model model) {
        userRepository.deleteById(id);

        return REDIRECT + "/";
    }

    @GetMapping("/edit/{id}")
    public String getEditPage(@PathVariable("id") Long id, Model model) {
        User existingUser = userRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing user with id: %s", id)));
        });


        model.addAttribute("user", existingUser);

        return UPDATE_USER;
    }

    @PostMapping("/update/{id}")
    public String updateUseradnReturnToIndex(@PathVariable("id") Long id,
                                             @Valid User user,
                                             BindingResult result) {
        if (result.hasErrors()) {
            return UPDATE_USER;
        }

        userRepository.save(user);
        return REDIRECT + "/";
    }

    static class Routes {
        static final String INDEX = "index";
        static final String ADD_USER = "add-user";
        static final String REDIRECT = "redirect:";
        static final String UPDATE_USER = "update-user";
    }

}
