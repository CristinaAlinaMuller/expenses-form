package alina.project.expense.controllers;

import alina.project.expense.repositories.ExpenseReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static alina.project.expense.controllers.LogController.Routes.*;


@Controller
public class LogController {
    @Autowired
    private ExpenseReportRepository expenseReportRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(LogController.class);

    @GetMapping("/login")
    public String getLoginPage(Model model) {

        LOGGER.info("Am primit un request de login");

        model.addAttribute("expenseReports", expenseReportRepository.findAll());
        return LOGIN;
    }

    static class Routes {
        static final String LOGIN = "login";
    }
}
