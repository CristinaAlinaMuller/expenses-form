package alina.project.expense.controllers;

import alina.project.expense.models.ExpenseReport;
import alina.project.expense.models.Document;
import alina.project.expense.models.User;
import alina.project.expense.repositories.ExpenseReportRepository;
import alina.project.expense.repositories.DocumentRepository;
import alina.project.expense.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static alina.project.expense.controllers.ContentController.Routes.*;
import static alina.project.expense.controllers.UserController.Routes.REDIRECT;

@Controller
public class ContentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExpenseReportRepository expenseReportRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @GetMapping("/see-Exp-reports/{id}")    // ADMIN only
    public String seeExpenseReportsOfUser(Model model, @PathVariable("id") Long id, User user) {
        User selectedUser = userRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing user with id: %s", id)));
        });

        model.addAttribute("expReports", selectedUser.getExpenseReports());
        model.addAttribute("userName", selectedUser.getLogin());

        return SEE_EXPENSE_ACCS;
    }

    @GetMapping("/expenseReport-details/{id}")        // ADMIN only
    public String seeExpenseReportDetails(Model model, @PathVariable("id") Long id, ExpenseReport expenseReport) {
        ExpenseReport currentExpenseReport = expenseReportRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        model.addAttribute("expenseReport", currentExpenseReport);
        model.addAttribute("docs", currentExpenseReport.getDocuments());

        return "expenseReport-details";
    }

    @GetMapping("/new-expenseReport") //USER
    public String getBlankExpenseReport(Model model, ExpenseReport expenseReport) {

        populateExpenseReports(model);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = userRepository.findByLogin(auth.getName());
        expenseReport.setUser(currentUser);
        expenseReport.setAdvance(0D);
        expenseReport.setLeaveValue(0);
        expenseReport.setTotal(0D);
        expenseReport.setTotalDocs(0D);

        model.addAttribute("totalDocs", expenseReport.getTotalDocs());
        model.addAttribute("totalExpenseReport", expenseReport.getTotal());
        model.addAttribute("expReportId", expenseReport.getId());
        expenseReportRepository.save(expenseReport);

        return ADD_EXPENSE_ACC;
    }

    @GetMapping("/add-expenseReport/{id}") //USER
    public String getExpenseReportPage(Model model, @PathVariable("id") Long id, ExpenseReport expenseReport) {

        ExpenseReport existingExpenseReport = expenseReportRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        populateDocuments(model);

        Double totalDocs = existingExpenseReport.getDocuments().stream().
                map(v -> v.getValue()).
                mapToDouble(Double::doubleValue).
                sum();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = userRepository.findByLogin(auth.getName());
        existingExpenseReport.setUser(currentUser);
        existingExpenseReport.setTotalDocs(totalDocs);
        existingExpenseReport.setTotal(totalDocs - existingExpenseReport.getAdvance() + existingExpenseReport.getLeaveValue());

        expenseReportRepository.save(existingExpenseReport);
        populateExpenseReports(model);

        model.addAttribute("tempDocs", existingExpenseReport.getDocuments());
        model.addAttribute("expenseReport", existingExpenseReport);

        return ADD_EXPENSE_ACC;
    }

    @PostMapping("/add-expenseReport/{id}") //USER
    public String createNewExpenseReport(@PathVariable("id") Long id,
                                      @Valid ExpenseReport expenseReport,
                                      BindingResult result,
                                      Model model) {

        if ((result.hasErrors())) {
            for (ObjectError temp : result.getAllErrors()) {
                LOGGER.error("found: {}", temp);
            }
            return ADD_EXPENSE_ACC;
        }

        ExpenseReport existingExpenseReport = expenseReportRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        Double totalDocs = existingExpenseReport.getDocuments().stream().
                map(v -> v.getValue()).
                mapToDouble(Double::doubleValue).
                sum();

        expenseReportRepository.save(expenseReport);
        Double a = expenseReport.getAdvance();
        int l = expenseReport.getLeaveValue();

        expenseReport.setNoOfDays(l / 50);
        expenseReport.setTotalDocs(totalDocs);
        expenseReport.setTotal(totalDocs + l - a);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = userRepository.findByLogin(auth.getName());
        expenseReport.setUser(currentUser);
        expenseReportRepository.save(expenseReport);

        populateExpenseReports(model);

        return REDIRECT + "/";
    }

    @GetMapping("/delete-expenseReport/{id}") //USER
    public String deleteExpenseReport(@PathVariable("id") Long id, Model model) {

        expenseReportRepository.deleteById(id);

        model.addAttribute("expenseReports", expenseReportRepository.findAll());

        return REDIRECT + "/";
    }

    @GetMapping("/back")
    public String goBack() {

        return REDIRECT;
    }

    @GetMapping("/back_newExp/{id}")
    public String goBackfromNewExp(@PathVariable("id") Long id) {
        ExpenseReport existingExpenseReport = expenseReportRepository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException((String.format("Missing expenseReport with id: %s", id)));
        });

        if (existingExpenseReport.getAdvance().equals(0D) &&
                existingExpenseReport.getLeaveValue() == 0 &&
                existingExpenseReport.getTotal().equals(0D) &&
                existingExpenseReport.getTotal().equals(0D)) {

            expenseReportRepository.delete(existingExpenseReport);
        }

        return REDIRECT + "/";
    }

    private void populateExpenseReports(Model model) {
        List<ExpenseReport> expenseReports = expenseReportRepository.findAll();
        if (!expenseReports.isEmpty()) {
            model.addAttribute("expenseReports", expenseReports);
        }
    }

    private void populateDocuments(Model model) {
        List<Document> documents = documentRepository.findAll();
        if (!documents.isEmpty()) {
            model.addAttribute("documents", documents);
        }
    }

    static class Routes {
        static final String ADD_EXPENSE_ACC = "add-expenseReport";
        static final String REDIRECT = "redirect:";
        static final String ADD_DOCUMENT = "add-document";
        static final String EDIT_DOC = "edit-doc";
        static final String UPDATE_DOC = "update-doc";
        static final String SEE_EXPENSE_ACCS = "see-Exp-reports";
    }

}
