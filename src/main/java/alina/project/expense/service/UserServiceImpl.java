package alina.project.expense.service;

import alina.project.expense.models.Role;
import alina.project.expense.models.User;
import alina.project.expense.models.UserRegistrationDto;
import alina.project.expense.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User findByLogin(String username) {
        return userRepository.findByLogin(username);
    }

    @Override
    public User save(UserRegistrationDto dto) {
        User userToSave = new User();
        userToSave.setLogin(dto.getLogin());
        userToSave.setEmail(dto.getEmail());
        userToSave.setPassword(passwordEncoder.encode(dto.getPassword()));
        userToSave.setRoles(Arrays.asList(new Role("USER_ROLE")));


        return userRepository.save(userToSave);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User existingUser = userRepository.findByLogin(username);

        if (existingUser == null) {
            throw new UsernameNotFoundException("Invalid username or password provided");
        }

        return new org.springframework.security.core.userdetails.User(
                existingUser.getLogin(), existingUser.getPassword(),
                mapRolesToAuthorities(existingUser.getRoles())
        );
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());

    }
}
