package alina.project.expense.service;

import alina.project.expense.models.User;
import alina.project.expense.models.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User findByLogin(String username);

    User save(UserRegistrationDto dto);


}
