package alina.project.expense.validation;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldMAtchValidator implements ConstraintValidator<FieldMatch, Object> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldMAtchValidator.class);
    private String firstNameField;
    private String secondNameField;

    public void initialize(final FieldMatch annotaion) {
        this.firstNameField = annotaion.first();
        this.secondNameField = annotaion.second();
    }

    @Override
    public boolean isValid(Object receivedObject, ConstraintValidatorContext constraintValidatorContext) {
        try {
            final Object firstObject = BeanUtils.getProperty(receivedObject, firstNameField);
            final Object secondObject = BeanUtils.getProperty(receivedObject, secondNameField);

            return firstObject == null && secondObject == null
                    || firstObject != null && firstObject.equals(secondObject);


        } catch (Exception e) {

            LOGGER.error("Am primit o exceptie la validarea campurilor : {}, {}", firstNameField, secondNameField, e);
            return false;

        }
    }

}
