package alina.project.expense.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

@Target({
        TYPE,
        ANNOTATION_TYPE
})

//cat timp se pastreaza in memorie obiectele anotate
@Retention(RUNTIME)
//marchez ca toolurile de tip javadoc ar trebui sa imi genereze documentatie si pt anotarea asta
@Documented
@Constraint(validatedBy = alina.project.expense.validation.FieldMAtchValidator.class)
public @interface FieldMatch {
    String first();
    String second();
    String message()default "{constraint.field-match}";

    Class <?> [] groups() default {};
    Class <? extends Payload> [] payload() default {};


    @Target({
            TYPE,
            ANNOTATION_TYPE
    })
    @Retention(RUNTIME)
    @Documented
    @interface List{
        FieldMatch[] value();
    }

}
