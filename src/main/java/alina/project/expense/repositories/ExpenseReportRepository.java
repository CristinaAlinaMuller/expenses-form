package alina.project.expense.repositories;

import alina.project.expense.models.ExpenseReport
        ;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpenseReportRepository extends JpaRepository<ExpenseReport, Long> {
}
